package main

import (
	"os"

	"poc.com/controllers"
	"poc.com/middleware"
	"poc.com/services"

	"poc.com/initializers"

	"github.com/gin-gonic/gin"
)

func init() {
	initializers.LoadEnvVariables()
	initializers.ConnectToDB()
}

func main() {

	go loadKafkaSubscribes()

	r := gin.Default()

	loadPostRotes(r)
	loadUserRotes(r)

	r.Run()
}

func loadPostRotes(r *gin.Engine) {
	r.POST("/posts", controllers.PostsCreate)
	r.GET("/posts", controllers.PostsAll)
	r.GET("/posts/:id", controllers.PostsShow)
	r.PUT("/posts/:id", controllers.PostsUpdate)
	r.DELETE("/posts/:id", controllers.PostsDelete)
}

func loadUserRotes(r *gin.Engine) {
	r.POST("/signup", controllers.Signup)
	r.POST("/login", controllers.Login)
	r.GET("/validate", middleware.RequireAuth, controllers.Validate)
}

func loadKafkaSubscribes() {
	topicCreateUser := os.Getenv("TOPIC_CREATE_USER")

	services.CreateUserSubscribe(topicCreateUser)

}
