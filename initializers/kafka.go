package initializers

import (
	"fmt"
	"os"

	"github.com/confluentinc/confluent-kafka-go/v2/kafka"
)

func KafkaProducer(topic string, message string) {
	broker := os.Getenv("KAFKA_BROKER")
	p, err := kafka.NewProducer(&kafka.ConfigMap{"bootstrap.servers": broker})
	if err != nil {
		panic(err)
	}

	defer p.Close()
	fmt.Printf("Send message: %v\n", message)
	p.Produce(&kafka.Message{
		TopicPartition: kafka.TopicPartition{Topic: &topic, Partition: kafka.PartitionAny},
		Value:          []byte(message),
	}, nil)

	// Wait for message deliveries before shutting down
	p.Flush(15 * 1000)
}

func KafkaConsumer() *kafka.Consumer {
	broker := os.Getenv("KAFKA_BROKER")
	c, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers":        broker,
		"group.id":                 "default",
		"auto.offset.reset":        "earliest",
		"allow.auto.create.topics": "true",
	})

	if err != nil {
		panic(err)
	}

	return c
}
