package clients

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

type AddressResponse struct {
	City   string `json:"localidade"`
	State  string `json:"uf"`
	Street string `json:"logradouro"`
}

func GetAddressByCep(areaCode string) AddressResponse {
	viaCepHost := fmt.Sprintf("https://viacep.com.br/ws/%s/json/", areaCode)

	address := AddressResponse{}

	fmt.Printf(viaCepHost)

	resp, getErr := http.Get(viaCepHost)
	if getErr != nil {
		log.Fatal(getErr)
	}

	body, readErr := ioutil.ReadAll(resp.Body)
	if readErr != nil {
		log.Fatal(readErr)
	}

	jsonErr := json.Unmarshal(body, &address)
	if jsonErr != nil {
		log.Fatal(jsonErr)
	}

	return address
}
