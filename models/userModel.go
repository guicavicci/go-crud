package models

import (
	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	Email    string `gorm:"unique"`
	Password string `json:"email"`
	AreaCode string `json:"areaCode"`
	City     string
	State    string
	Street   string
}
