FROM golang:1.20.2

RUN apt-get update && \
apt-get install build-essential -y

# RUN update ca-certificates
RUN apt-get update && apt-get upgrade -y && apt-get install pkgconf git bash -y
RUN git clone https://github.com/edenhill/librdkafka.git && cd librdkafka && ./configure --prefix /usr && make && make install

ENV PROJECT_DIR=/app \
    GO111MODULE=on \
    CGO_ENABLED=1 \
    GOOS=linux \
    GOARCH=amd64

WORKDIR /app
RUN mkdir "/build"
COPY . .
RUN go mod download
RUN go install github.com/githubnemo/CompileDaemon
ENTRYPOINT CompileDaemon -build="go build -o /build/app" -command="/build/app"