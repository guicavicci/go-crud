package controllers

import (
	"net/http"
	"strconv"

	"poc.com/services"

	"github.com/gin-gonic/gin"
)

func PostsCreate(c *gin.Context) {
	var postReq services.Post

	//Get data of req body
	c.Bind(&postReq)

	//Create a post
	post, err := postReq.Create()

	if err != nil {
		c.Status(400)
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"post": post,
	})
}

func PostsAll(c *gin.Context) {
	var postReq services.Post

	page := 1

	//Get page number
	pageStr := c.Query("page")
	if pageStr != "" {
		page, _ = strconv.Atoi(pageStr)
	}
	offset := (page - 1) * 5

	//Get the posts
	posts := postReq.ListAll(offset)

	//Respond
	c.JSON(http.StatusOK, gin.H{
		"post": posts,
	})
}

func PostsShow(c *gin.Context) {
	var postReq services.Post
	//Get id from URL
	id := c.Param("id")

	//Get the posts
	post := postReq.GetById(id)

	//Respond
	c.JSON(http.StatusOK, gin.H{
		"post": post,
	})
}

func PostsUpdate(c *gin.Context) {
	var postReq services.Post
	//Get id from URL
	id := c.Param("id")

	//Get data of request body
	c.Bind(&postReq)

	//Get the post and update
	post, err := postReq.Update(id)

	if err != nil {
		c.Status(400)
		return
	}

	//Respond
	c.JSON(http.StatusOK, gin.H{
		"post": post,
	})
}

func PostsDelete(c *gin.Context) {
	var postReq services.Post
	//Get id from URL
	id := c.Param("id")

	//Get the post
	postReq.DeleteById(id)

	//Respond
	c.Status(http.StatusNoContent)
}
