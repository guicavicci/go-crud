package main

import (
	"poc.com/initializers"
	"poc.com/models"
)

func init() {
	initializers.LoadEnvVariables()
	initializers.ConnectToDB()
}

func main() {
	db := initializers.ConnectToDB()
	db.AutoMigrate(&models.Post{}, &models.User{})
}
