# Posts
## Definition
 Posts crud project using postgres relational database
 **Programming language:** Go
 **Frameworks:** :
- Gin
- Gorm

 **Commands to create the project**
```bash
go mod init poc.com
go get github.com/githubnemo/CompileDaemon
go install github.com/githubnemo/CompileDaemon
go get github.com/joho/godotenv
go get github.com/gin-gonic/gin
go get -u gorm.io/gorm
go get -u gorm.io/driver/postgres
go get -u golang.org/x/crypto/bcrypt
go get -u github.com/golang-jwt/jwt/v4
go get -u github.com/confluentinc/confluent-kafka-go/v2/kafka
go get -u github.com/go-mail/mail
```

**Commands to inicialize**
CompileDaemon -command="./go-crud"