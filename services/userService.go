package services

import (
	"errors"
	"log"

	"golang.org/x/crypto/bcrypt"
	"poc.com/clients"
	"poc.com/models"
)

type UserPojo struct {
	Email    string
	Password string
	AreaCode string
}

type UserService interface {
	Signup() error
}

func (userPojo UserPojo) Signup() error {

	// Hash the password
	hash, err := bcrypt.GenerateFromPassword([]byte(userPojo.Password), 10)

	if err != nil {
		log.Printf("error in crypt Password")
		return errors.New("Failed to hash password")
	}

	user := models.User{Email: userPojo.Email, Password: string(hash), AreaCode: userPojo.AreaCode}

	address := clients.GetAddressByCep(user.AreaCode)
	user.City = address.City
	user.State = address.State
	user.Street = address.Street

	//Create the user
	db := getDBConnection()
	result := db.Create(&user)

	if result.Error != nil {
		log.Printf("error in create User")
		return errors.New("error in create User")
	}

	go CreateUserProducer(user.Email)

	return nil

}
