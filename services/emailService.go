package services

import (
	"os"

	"github.com/go-mail/mail"
	"poc.com/models"
)

func SendEmail(email models.Email) {
	emailFrom := os.Getenv("EMAIL_FROM")
	m := mail.NewMessage()

	m.SetHeader("From", emailFrom)
	m.SetHeader("To", email.To)
	m.SetHeader("Subject", "Hello!")
	m.SetBody("text/html", email.Body)

	//@TODO
	// d := mail.NewDialer("smtp.gmail.com", 587, emailFrom, "Mudar@123")

	// if err := d.DialAndSend(m); err != nil {

	// 	panic(err)

	// }

}
