package services

import (
	"errors"
	"log"
	"poc.com/models"
)

type PostService interface {
	Create() (models.Post, bool)
	PostsAll() []models.Post
	PostsShow() models.Post
	PostsUpdate() models.Post
	PostsDelete()
}

type Post struct {
	Title string
	Body  string
}

func (postRepresentation Post) Create() (models.Post, error) {
	post := models.Post{Title: postRepresentation.Title, Body: postRepresentation.Body}

	db := getDBConnection()
	result := db.Create(&post)

	if result.Error != nil {
		log.Fatalf("error in create Post")
		return post, errors.New("error in create Post")
	}

	return post, nil

}

func (postRepresentation Post) ListAll(offset int) []models.Post {
	var posts []models.Post
	db := getDBConnection()

	db.Limit(5).Offset(offset).Find(&posts)

	return posts
}

func (postRepresentation Post) GetById(id string) models.Post {
	var post models.Post
	db := getDBConnection()
	db.First(&post, id)

	return post
}

func (postRepresentation Post) Update(id string) (models.Post, error) {

	db := getDBConnection()

	//Get the post
	var post models.Post
	db.First(&post, id)

	//Update
	result := db.Model(&post).Updates(models.Post{
		Title: postRepresentation.Title,
		Body:  postRepresentation.Body,
	})

	if result.Error != nil {
		log.Fatalf("Error in update Post")
		return post, errors.New("Error in update Post")
	}

	return post, nil

}

func (postRepresentation Post) DeleteById(id string) {
	db := getDBConnection()
	db.Delete(&models.Post{}, id)
}