package services

import (
	"gorm.io/gorm"
	"poc.com/initializers"
)

func getDBConnection() *gorm.DB {
	return initializers.ConnectToDB()
}
