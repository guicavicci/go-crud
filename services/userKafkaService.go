package services

import (
	"fmt"
	"os"

	"github.com/confluentinc/confluent-kafka-go/v2/kafka"
	"poc.com/initializers"
	"poc.com/models"
)

func CreateUserProducer(message string) {
	topicCreateUser := os.Getenv("TOPIC_CREATE_USER")
	initializers.KafkaProducer(topicCreateUser, message)

}

func CreateUserSubscribe(topic string) {
	c := initializers.KafkaConsumer()

	c.SubscribeTopics([]string{topic}, nil)

	for {
		msg, err := c.ReadMessage(-1)
		if err == nil {
			msgString := string(msg.Value)
			fmt.Printf("Message: %s received on topic: %s\n", msgString, topic)
			sendMail(msgString)
		} else if !err.(kafka.Error).IsTimeout() {
			fmt.Printf("Consumer error: %v (%v)\n", err, msg)
		}
	}
	c.Close()
}

func sendMail(emailAddress string) {

	email := models.Email{
		Subject: "User created",
		To:      emailAddress,
		Body:    "user created successfully"}
	SendEmail(email)
}
